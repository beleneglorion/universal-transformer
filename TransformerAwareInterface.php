<?php
declare(strict_types=1);

namespace BusinessDecision\Component\Transformer;

interface TransformerAwareInterface {


    /**
     * @param TransformerManagerInterface $transformer
    */
    public function setTransformer(TransformerManagerInterface $transformer);

}
