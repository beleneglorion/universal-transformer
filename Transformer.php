<?php
declare(strict_types=1);

namespace BusinessDecision\Component\Transformer;

use BusinessDecision\Component\Transformer\Context\ContextInterface;
use BusinessDecision\Component\Transformer\Exception\UnsupportedTransformationException;
use BusinessDecision\Component\Transformer\Transformer\CacheableSupportsMethodInterface;


class Transformer implements TransformerManagerInterface
{

    /**
     * @var TransformerInterface[]
     */
    protected $transformers;

    /**
     * @var array
     */
    protected $transformerCache = [];

    /**
     * @var array
     */
    protected $cachedTransformers = [];

    /**
     * Transformer constructor.
     *
     * @param TransformerInterface[]|\Traversable $transformers
     */
    public function __construct($transformers = [])
    {


        foreach ($transformers as $transformer) {
            if ($transformer instanceof TransformerAwareInterface) {
                $transformer->setTransformer($this);
            }
            $this->transformers[] = $transformer;
        }
    }

    /**
     * @param array|object|\Traversable $data
     * @param string                    $targetClass
     * @param ContextInterface|null     $context
     *
     * @return bool
     */
    public function supports($data, $targetClass, ContextInterface $context = null): bool
    {

        return null !== $this->getTransformer($data, $targetClass, $context);
    }


    /**
     * @param array|object|\Traversable $data
     * @param string                    $targetClass
     * @param ContextInterface|null     $context
     *
     * @return array|object|object[]
     */
    public function transform($data, $targetClass, ContextInterface $context = null)
    {

        $transformer =$this->getTransformer($data, $targetClass, $context);
        if (null === $transformer) {
            throw new UnsupportedTransformationException(sprintf(
                'Can not transform data of type "%s" to object of type "%s"',
                is_object($data) ? get_class($data) : gettype($data),
                $targetClass
            ));
        }

        return $transformer->transform($data, $targetClass, $context);
    }

    /**
     * @param mixed            $data
     * @param string           $targetClass
     * @param ContextInterface $context
     *
     * @return TransformerInterface|null
     */
    private function getTransformer($data, string $targetClass, ContextInterface $context = null): ?TransformerInterface
    {
        if ($this->cachedTransformers !== $this->transformers) {
            $this->cachedTransformers = $this->transformers;
            $this->transformerCache = array();
        }

        $type = \is_object($data) ? \get_class($data) : 'native-'.\gettype($data);
        if (!isset($this->transformerCache[$targetClass][$type])) {
            $this->transformerCache[$targetClass][$type] = array();
            foreach ($this->transformers as $k => $transformer) {
                if (!$transformer instanceof TransformerInterface) {
                    continue;
                }
                if (!$transformer instanceof CacheableSupportsMethodInterface || !$transformer->hasCacheableSupportsMethod()) {
                    $this->transformerCache[$targetClass][$type][$k] = false;
                } elseif ($transformer->supports($data, $targetClass)) {
                    $this->transformerCache[$targetClass][$type][$k] = true;
                    break;
                }
            }
        }

        foreach ($this->transformerCache[$targetClass][$type] as $k => $cached) {
            $transformer = $this->transformers[$k];
            if ($cached || $transformer->supports($data, $targetClass, $context)) {
                return $transformer;
            }
        }

        return null;
    }
}
