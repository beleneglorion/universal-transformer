# Universal Transformer

[![License][license-image]][license-link]

[license-image]: https://img.shields.io/dub/l/vibe-d.svg
[license-link]: https://github.com/symfony-notes/object-transformer-bundle/blob/master/LICENSE


Inspired from work for [Roman Zhuravel](https://github.com/symfony-notes/object-transformer-bundle)
