<?php
declare(strict_types=1);


namespace BusinessDecision\Component\Transformer\Context;


/**
 * Interface for represents object transformation context.
 */
interface ContextInterface
{
    /**
     *
     *
     * @return array|null
     */
    public function getPayload(): ?array;
}
