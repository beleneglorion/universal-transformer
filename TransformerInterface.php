<?php
declare(strict_types=1);

namespace BusinessDecision\Component\Transformer;

use BusinessDecision\Component\Transformer\Context\ContextInterface;

interface TransformerInterface {

    /**
     * Does transformer support transformation to target class?
     *
     * @param object|array|\Traversable $data
     * @param string                    $targetClass
     * @param ContextInterface|null     $context
     *
     * @return bool
     */
    public function supports($data, $targetClass, ContextInterface $context = null): bool;

    /**
     * Transforms $data to supported class with specified context.
     *
     * @param object|array|\Traversable $data
     * @param string                    $targetClass
     * @param ContextInterface|null     $context
     *
     * @return array|object|\object[]
     */
    public function transform($data, $targetClass, ContextInterface $context = null);

}
