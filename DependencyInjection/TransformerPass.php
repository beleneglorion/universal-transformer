<?php

declare(strict_types=1);


namespace BusinessDecision\Component\Transformer\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use BusinessDecision\Component\Transformer\TransformerManagerInterface;

class TransformerPass implements CompilerPassInterface
{
    const TRANSFORMER_TAG = 'transformer.extensions';


    use PriorityTaggedServiceTrait;

    private $transformerTag;


    public function __construct(
        string $transformerTag = self::TRANSFORMER_TAG
    ) {
        $this->transformerTag = $transformerTag;
    }

    public function process(ContainerBuilder $container)
    {
        $transformerService = TransformerManagerInterface::class;
        if (!$container->hasDefinition($transformerService)) {
            return;
        }

        if (!$transformers = $this->findAndSortTaggedServices($this->transformerTag, $container)) {
            throw new RuntimeException(
                sprintf(
                    'You must tag at least one service as "%s" to use the "%s" service.',
                    $this->transformerTag,
                    $transformerService
                )
            );
        }
        $definition = $container->getDefinition($transformerService);
        $definition->setArgument(0, $transformers);
    }
}
