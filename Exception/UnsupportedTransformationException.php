<?php
namespace BusinessDecision\Component\Transformer\Exception;


class UnsupportedTransformationException extends \RuntimeException
{
}
